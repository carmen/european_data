import pandas as pd

def load_csv(path, index_col=None):
    
    if index_col is None:
        df=pd.read_csv(path, index_col=0)
    
    elif index_col =='UTC':
        df=pd.read_csv(path)
        if 'UTC' in df.columns:
            df['UTC']=pd.to_datetime(df['UTC'])
            df=df.set_index('UTC')
        else:
            df=pd.read_csv(path, index_col=0)
            df.index=pd.to_datetime(df.index)
            df.index.name='UTC'
    
    else:
        df=pd.read_csv(path)
        df=df.set_index(index_col)
    
    return df
        
